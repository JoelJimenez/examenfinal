package pe.uni.jjimenezch.examenfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Resources;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
public class MainActivity extends AppCompatActivity {

    Button next;
    Button back;
    Button isTrue;
    Button isFalse;
    int cont;
    ArrayList<Question> questions;

    //
    TextView question;
    ImageView imageQuestion;
    boolean correctAnswer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        question = findViewById(R.id.titulo);
        imageQuestion = findViewById(R.id.imageView1);
        next = findViewById(R.id.next);
        back = findViewById(R.id.back);
        isTrue = findViewById(R.id.isTrue);
        isFalse = findViewById(R.id.isFalse);

        cont = 0;
        initQuestionArray();
        setQuestionInView();

        next.setOnClickListener(view -> {
            if (cont + 1 < questions.size()) {
                cont++;
            } else {
                cont = 0;
            }
            setQuestionInView();
        });

        back.setOnClickListener(view -> {
            if (cont - 1 > -1) {
                cont--;
                setQuestionInView();
            }
        });

        isFalse.setOnClickListener(view -> {
            RelativeLayout relativeLayout;
            relativeLayout = findViewById(R.id.relative);

            int message;
            if (!correctAnswer) {
                message = R.string.Correcto;
            } else {
                message = R.string.Incorrrecto;
            }

            Snackbar.make(relativeLayout, message, Snackbar.LENGTH_SHORT).show();
        });

        isTrue.setOnClickListener(view -> {
            RelativeLayout relativeLayout;
            relativeLayout = findViewById(R.id.relative);

            int message;
            if (correctAnswer) {
                message = R.string.Correcto;
            } else {
                message = R.string.Incorrrecto;
            }

            Snackbar.make(relativeLayout, message, Snackbar.LENGTH_SHORT).show();
        });

    }

    void initQuestionArray(){
        questions = new ArrayList<>();

        Resources res = getResources();
        questions.add(new Question(String.valueOf(res.getString(R.string.pregunta1)), R.drawable.perro, true));
        questions.add(new Question(String.valueOf(res.getString(R.string.pregunta2)), R.drawable.tigre, false));
        questions.add(new Question(String.valueOf(res.getString(R.string.pregunta3)), R.drawable.jirafa, true));
        questions.add(new Question(String.valueOf(res.getString(R.string.pregunta4)), R.drawable.loro, false));
        questions.add(new Question(String.valueOf(res.getString(R.string.pregunta5)), R.drawable.gato, true));
        questions.add(new Question(String.valueOf(res.getString(R.string.pregunta6)), R.drawable.jirafa, false));
    }

    void setQuestionInView(){

        question.setText(questions.get(cont).question);
        imageQuestion.setImageResource(questions.get(cont).imageId);
        correctAnswer = questions.get(cont).answer;
    }
}